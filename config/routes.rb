# frozen_string_literal: true

Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'categories/index'
  get 'categories/show'
  get 'responses/create'
  root 'static_pages#home'
  get 'static_pages/help'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :topics, only: %i(show index create)
  resources :responses, only: [:create]
  resources :categories, only: [:show]
end
