# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Topic, type: :model do
  # FactoryBot set up
  before do
    @topic = create(:example_topic)
  end

  it 'is valid' do
    expect(@topic.valid?).to eq(true)
  end

  it 'title shoule be present' do
    @topic.title = ''
    expect(@topic.valid?).to eq(false)
  end

  it 'user shoule be present' do
    @topic.user = nil
    expect(@topic.valid?).to eq(false)
  end

  # 64文字以下
  it 'the length of title shoule not be too long' do
    @topic.title = 'a' * 65
    expect(@topic.valid?).to eq(false)
  end
end
