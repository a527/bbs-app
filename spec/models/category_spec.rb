# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Category, type: :model do
  # FactoryBot set up
  before do
    @category = create(:example_category)
  end

  it 'is valid' do
    expect(@category.valid?).to eq(true)
  end

  it 'title shoule be present' do
    @category.title = nil
    expect(@category.valid?).to eq(false)
  end

  it 'title shoule not be too long' do
    @category.title = 'a' * 65
    expect(@category.valid?).to eq(false)
  end

  it 'content shoule be present' do
    @category.content = nil
    expect(@category.valid?).to eq(false)
  end

  it 'content shoule not be too long' do
    @category.content = 'a' * 257
    expect(@category.valid?).to eq(false)
  end
end
