# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Response, type: :model do
  # FactoryBot set up
  before do
    @response = create(:example_response)
  end

  it 'is valid' do
    expect(@response.valid?).to eq(true)
  end

  it 'topic shoule be present' do
    @response.topic = nil
    expect(@response.valid?).to eq(false)
  end

  it 'user shoule be present' do
    @response.user = nil
    expect(@response.valid?).to eq(false)
  end

  # 1024文字以下
  it 'the length of title shoule not be too long' do
    @response.content = 'a' * 1025
    expect(@response.valid?).to eq(false)
  end
end
