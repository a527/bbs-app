# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  # FactoryBot set up
  before do
    @user = build(:example_user)
  end

  describe 'Validation' do
    it 'is valid' do
      expect(@user.valid?).to eq(true)
    end

    # メールアドレスは必須
    it 'email shoule be present' do
      @user.email = ' '
      expect(@user.valid?).to eq(false)
    end

    # メールアドレスは一意
    it 'email should be unique' do
      duplicate_user = @user.dup
      duplicate_user.email = @user.email.upcase
      @user.save
      expect(duplicate_user.valid?).to eq(false)
    end

    # 有効なメールアドレス
    it 'email validation should accept valid addresses' do
      valid_addresses = %w(
        user@example.com USER@foo.COM A_US-ER@foo.bar.org
        first.last@foo.jp alice+bob@baz.cn
      )
      valid_addresses.each do |valid_address|
        @user.email = valid_address
        expect(@user.valid?).to eq(true), "#{valid_address.inspect} should be valid"
      end
    end

    # 有効ではないメールアドレス
    it 'email validation should reject invalid addresses' do
      invalid_addresses = %w(
        user@example,com user_at_foo.org user.name@example.
        foo@bar_baz.com foo@bar+baz.com
      )
      invalid_addresses.each do |invalid_address|
        @user.email = invalid_address
        expect(@user.valid?).to eq(false), "#{invalid_address.inspect} should be invalid"
      end
    end

    # パスワードは必須
    it 'password should not be blank' do
      @user.password = @user.password_confirmation = ' ' * 6
      expect(@user.valid?).to eq(false)
    end

    # パスワードは6文字以上
    it 'password should not be too short' do
      @user.password = @user.password_confirmation = 'a' * 5
      expect(@user.valid?).to eq(false)
    end
  end
end
