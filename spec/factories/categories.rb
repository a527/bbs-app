# frozen_string_literal: true

FactoryBot.define do
  factory :example_category, class: Category do
    title { 'cat' }
    content { 'This is test category' }
  end

  factory :another_category, class: Category do
    title { 'dog' }
    content { 'This is another category' }
  end
end
