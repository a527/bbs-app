# frozen_string_literal: true

FactoryBot.define do
  factory :relationship do
    category { nil }
    topic { nil }
  end
end
