# frozen_string_literal: true

FactoryBot.define do
  factory :example_response, class: Response do
    association :user, factory: :jerry
    association :topic, factory: :example_topic
    content { 'MyText' }
  end
end
