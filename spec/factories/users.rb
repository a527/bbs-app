# frozen_string_literal: true

FactoryBot.define do
  factory :example_user, class: User do
    email { 'example@example.com' }
    password { 'password' }
  end

  factory :jerry, class: User do
    email { 'jerry@example.com' }
    password { 'password' }
  end
end
