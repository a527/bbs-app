# frozen_string_literal: true

FactoryBot.define do
  factory :example_topic, class: Topic do
    title { 'rspecについて' }
    association :user, factory: :example_user
  end
end
