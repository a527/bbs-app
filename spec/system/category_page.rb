# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'レス作成', type: :system do
  let!(:example_user) { create(:example_user) }
  let!(:category) { create(:example_category) }
  let!(:another_category) { create(:another_category) }

  describe 'カテゴリーページ' do
    before do
      sign_in example_user
      visit root_path
      fill_in 'title', with: 'test_title'
      fill_in 'content', with: 'test_content'
      find('label', text: category.title).click
      click_on 'スレッドを作成'

      visit root_path
      fill_in 'title', with: 'another'
      fill_in 'content', with: 'another'
      find('label', text: another_category.title).click
      click_on 'スレッドを作成'
    end

    example '適切なスレッドが表示されていること' do
      visit category_path(category)
      expect(page).to have_content 'test_title'

      visit category_path(another_category)
      expect(page).to have_content 'another'
    end

    example '該当しないスレッドが表示されていないこと' do
      visit category_path(category)
      expect(page).not_to have_content 'another'

      visit category_path(another_category)
      expect(page).not_to have_content 'test_title'
    end
  end
end
