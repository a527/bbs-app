# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'スレッド作成', type: :system do
  let!(:example_user) { create(:example_user) }
  let!(:category) { create(:example_category) }

  describe 'スレッド作成' do
    context 'ログインユーザーの場合' do
      before do
        sign_in example_user
        visit root_path
      end

      context '適切なスレッド作成' do
        example 'スレッドの情報が表示されていること' do
          fill_in 'title', with: 'test_title'
          fill_in 'content', with: 'test_content'
          find('label', text: category.title).click
          click_on 'スレッドを作成'
          expect(page).to have_content 'test_title'
          expect(page).to have_content 'test_content'
        end
      end
    end
  end
end
