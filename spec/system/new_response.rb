# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'レス作成', type: :system do
  let!(:example_user) { create(:example_user) }
  let!(:category) { create(:example_category) }

  describe 'スレッド画面からのレス作成' do
    before do
      sign_in example_user
      visit root_path
      fill_in 'title', with: 'test_title'
      fill_in 'content', with: 'test_content'
      find('label', text: category.title).click
      click_on 'スレッドを作成'
    end

    context '適切なレスポンス作成' do
      example 'スレッドの情報が表示されていること' do
        fill_in 'response[content]', with: 'test_response'
        click_on '投稿'
        expect(page).to have_content 'test_response'
      end
    end

    context '適切でないレスポンス作成' do
      example 'スレッドの情報が表示されていないこと' do
        fill_in 'response[content]', with: ''
        click_on '投稿'
        expect(page).not_to have_content 'test_response'
      end
    end
  end

  describe 'ホーム画面からのレス作成' do
    before do
      sign_in example_user
      visit root_path
      fill_in 'title', with: 'test_title'
      fill_in 'content', with: 'test_content'
      find('label', text: category.title).click
      click_on 'スレッドを作成'
      visit root_path
    end

    context '適切なレスポンス作成' do
      example 'スレッドの情報が表示されていること' do
        fill_in 'response[content]', with: 'test_response'
        click_on '投稿'
        expect(page).to have_content 'test_response'
      end
    end
  end
end
