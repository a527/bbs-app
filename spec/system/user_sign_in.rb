# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'ログイン', type: :system do
  let(:example_user) { create(:example_user) }

  describe 'ログイン' do
    context '成功する場合' do
      it '正しいフラッシュが表示される' do
        visit new_user_session_path
        fill_in 'user[email]', with: example_user.email
        fill_in 'user[password]', with: example_user.password
        click_on 'ログイン'
        expect(page).to have_content 'Signed in successfully.'
      end
    end

    context '失敗する場合' do
      it 'エラーメッセージが表示される' do
        visit new_user_session_path
        click_on 'ログイン'
        expect(page).to have_content 'Invalid Email or password.'
      end
    end
  end
end
