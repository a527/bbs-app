# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Responses', type: :request do
  describe 'GET /create' do
    let(:topic) { create(:example_topic) }

    context 'Login User' do
      before do
        sign_in topic.user
      end

      it 'success' do
        post responses_path, params: { response: { content: 'test', topic_id: topic.id } }
        expect(response.status).to eq 302
        expect(response).to redirect_to root_path
      end
    end

    context 'ゲストの場合' do
      example 'ログイン画面に遷移' do
        post topics_path
        expect(response.status).to eq 302
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
