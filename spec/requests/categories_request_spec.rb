# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Categories', type: :request do
  describe 'GET /show' do
    let(:category) { create(:example_category) }
    let(:user) { create(:example_user) }

    context 'Login User' do
      before do
        sign_in user
      end

      it 'returns http success' do
        get category_path(category)
        expect(response).to have_http_status(:success)
      end
    end

    context 'ゲストの場合' do
      example 'ログイン画面に遷移' do
        post topics_path
        expect(response.status).to eq 302
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
