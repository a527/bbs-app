# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Topics', type: :request do
  describe 'GET /create' do
    let(:user) { create(:example_user) }
    let(:category) { create(:example_category) }

    context 'Login User' do
      before do
        sign_in user
      end

      it 'success' do
        post topics_path, params: { title: 'test', content: 'test', category_ids: ['', category.id.to_s] }
        expect(response.status).to eq 302
      end
    end

    context 'ゲストの場合' do
      example 'ログイン画面に遷移' do
        post topics_path
        expect(response.status).to eq 302
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'GET /index' do
    let(:user) { create(:example_user) }

    context 'Login User' do
      before do
        sign_in user
      end

      it 'success' do
        get topics_path
        expect(response.status).to eq 200
      end
    end

    context 'ゲストの場合' do
      example 'ログイン画面に遷移' do
        get topics_path
        expect(response.status).to eq 302
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'GET /show' do
    let(:topic) { create(:example_topic) }

    context 'Login User' do
      before do
        sign_in topic.user
      end

      it 'success' do
        get topic_path(topic)
        expect(response.status).to eq 200
      end
    end

    context 'ゲストの場合' do
      example 'ログイン画面に遷移' do
        get topic_path(topic)
        expect(response.status).to eq 302
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
