# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'StaticPages', type: :request do
  describe 'GET /home' do
    let(:user) { create(:example_user) }

    context 'Login User' do
      before do
        sign_in user
      end

      it 'returns http success' do
        get '/'
        expect(response).to have_http_status(:success)
      end
    end

    context 'Guest' do
      it 'ログイン画面に遷移する' do
        get '/'
        expect(response.status).to eq 302
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
