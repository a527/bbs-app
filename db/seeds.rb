# frozen_string_literal: true

50.times do |n|
  User.create!(
    email: "user-#{n + 1}@example.com",
    password: 'password'
  )
end

10.times do |_n|
  Category.create!(
    title: Faker::Food.unique.fruits,
    content: Faker::Food.measurement
  )
end

3.times do |user_index|
  5.times do |_n|
    user = User.find(user_index + 1)
    title = Faker::Lorem.sentence(5)
    content = Faker::ChuckNorris.fact
    topic = user.topics.create!(title: title)
    Relationship.create!(topic_id: topic.id, category_id: Category.first.id)
    user.responses.create!(content: content, topic_id: topic.id)
  end
end
