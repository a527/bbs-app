# frozen_string_literal: true

class AddPosterToResponses < ActiveRecord::Migration[5.2]
  def change
    add_column :responses, :poster, :string
  end
end
