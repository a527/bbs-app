# frozen_string_literal: true

class CreateResponses < ActiveRecord::Migration[5.2]
  def change
    create_table :responses do |t|
      t.references :user, foreign_key: true
      t.references :topic, foreign_key: true
      t.text :content

      t.timestamps
    end
  end
end
