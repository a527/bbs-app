# frozen_string_literal: true

class CreateRelationships < ActiveRecord::Migration[5.2]
  def change
    create_table :relationships do |t|
      t.references :category, foreign_key: true
      t.references :topic, foreign_key: true

      t.timestamps
    end
  end
end
