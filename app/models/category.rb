# frozen_string_literal: true

class Category < ApplicationRecord
  has_many :relationships
  has_many :topics, through: :relationships

  validates :title, presence: true, length: { maximum: 64 }, uniqueness: true
  validates :content, presence: true, length: { maximum: 256 }
end
