# frozen_string_literal: true

class Relationship < ApplicationRecord
  belongs_to :category
  belongs_to :topic

  validates :category_id, presence: true
  validates :topic_id, presence: true
end
