# frozen_string_literal: true

class Topic < ApplicationRecord
  belongs_to :user
  has_many :responses, dependent: :destroy
  has_many :relationships
  has_many :categories, through: :relationships

  default_scope -> { order(created_at: :desc) }

  validates :title, presence: true, length: { maximum: 64 }
  validates :user_id, presence: true
end
