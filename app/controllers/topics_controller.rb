# frozen_string_literal: true

class TopicsController < ApplicationController
  before_action :authenticate_user!, only: %i(create index show search)
  require 'will_paginate/array'

  # POST /topics
  def create
    @topic_form = TopicForm.new(new_topic_form_params)
    redirect_to topic_path(@topic_form.topic_id) if @topic_form.save!
  end

  # GET /topic/:id
  def show
    @categories = Category.all
    @topic = Topic.find(params[:id])
    @responses = @topic.responses.includes([:user])
  end

  # GET /topics
  def index
    @categories = Category.all
    @keyword = params[:keyword]
    @topics = Topic.where('title like ?', "%#{params[:keyword]}%").includes([:relationships]).includes([:categories])
    @responses = Topic.joins(:responses).where('content like ?', "%#{params[:keyword]}%").includes([:relationships]).includes([:categories])
    @results = @topics | @responses
    @results = @results.paginate(page: params[:page], per_page: 5)
  end

  private

  def new_topic_form_params
    params.permit(:title, :content, :poster, category_ids: []).merge(user_id: current_user.id)
  end
end
