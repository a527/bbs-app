# frozen_string_literal: true

class ResponsesController < ApplicationController
  before_action :authenticate_user!, only: [:create]

  def create
    @response = current_user.responses.build(response_params)

    redirect_to request.referrer || root_url if @response.save
  end

  private

  def response_params
    params.require(:response).permit(:content, :topic_id, :poster)
  end
end
