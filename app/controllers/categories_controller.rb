# frozen_string_literal: true

class CategoriesController < ApplicationController
  before_action :authenticate_user!, only: [:show]

  def show
    @categories = Category.all
    @category = Category.find(params[:id])
    @topics = @category.topics.includes([:relationships]).includes([:categories]).paginate(page: params[:page], per_page: 5)
  end
end
