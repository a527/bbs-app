# frozen_string_literal: true

class StaticPagesController < ApplicationController
  before_action :authenticate_user!, only: [:home]

  def home
    @topics = Topic.all.includes([:relationships]).includes([:categories]).paginate(page: params[:page], per_page: 5)
    @categories = Category.all
  end
end
