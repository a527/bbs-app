# frozen_string_literal: true

class TopicForm
  include ActiveModel::Model
  include ActiveModel::Attributes

  # user
  attr_accessor :user_id

  # topic
  attr_accessor :title

  # resoponse
  attr_accessor :content
  attr_accessor :topic_id
  attr_accessor :poster

  # category
  attr_accessor :category_ids

  # validation????

  def save!
    ActiveRecord::Base.transaction do
      return false if category_ids.last.empty?

      # user
      user = User.find(user_id)
      # topic
      topic = user.topics.build(title: title)
      return false unless topic.save

      # response
      self.topic_id = topic.id
      first_response = user.responses.build(content: content, topic_id: topic_id, poster: poster)
      return false unless first_response.save

      # relationship
      category_ids.each do |category_id|
        if category_id.present?
          relationship = Relationship.new(topic_id: topic.id, category_id: category_id)
          relationship.save!
        end
      end
    end
  end
end
